#!/usr/bin/env python
"""
NAME: 2ens.py
=============

DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.0.2    20200318 Extraction of external id if available.
0.0.1    2019     Initial version.

LICENCE
=======
2019, copyright Sebastian Schmeier
s.schmeier@pm.me // https://www.sschmeier.com

template version: 2.0 (2018/12/19)
"""
__version__ = "0.0.2"
__date__ = "2019"
__email__ = "s.schmeier@pm.me"
__author__ = "Sebastian Schmeier"

import sys
import os
import argparse
import csv
import gzip
import bz2
import zipfile
import time

# non-standard lib: For color handling on the shell
try:
    from colorama import init, Fore

    # INIT color
    # Initialise colours for multi-platform support.
    init()
    reset = Fore.RESET
    colors = {
        "success": Fore.GREEN,
        "error": Fore.RED,
        "warning": Fore.YELLOW,
        "info": "",
    }
except ImportError:
    sys.stderr.write(
        "colorama lib desirable. " + 'Install with "conda install colorama".\n\n'
    )
    reset = ""
    colors = {"success": "", "error": "", "warning": "", "info": ""}


def alert(atype, text, log, repeat=False, flush=False):
    if repeat:
        textout = "{} [{}] {}\r".format(
            time.strftime("%Y%m%d-%H:%M:%S"), atype.rjust(7), text
        )
    else:
        textout = "{} [{}] {}\n".format(
            time.strftime("%Y%m%d-%H:%M:%S"), atype.rjust(7), text
        )

    log.write("{}{}{}".format(colors[atype], textout, reset))
    if flush:
        log.flush()
    if atype == "error":
        sys.exit(1)


def success(text, log=sys.stderr, flush=True):
    alert("success", text, log, flush=flush)


def error(text, log=sys.stderr, flush=True):
    alert("error", text, log, flush=flush)


def warning(text, log=sys.stderr, flush=True):
    alert("warning", text, log, flush=flush)


def info(text, log=sys.stderr, repeat=False, flush=True):
    alert("info", text, log, repeat=repeat, flush=flush)


def parse_cmdline():
    """ Parse command-line args. """
    # parse cmd-line ----------------------------------------------------------
    description = "Read delimited file."
    version = "version {}, date {}".format(__version__, __date__)
    epilog = "Copyright {} ({})".format(__author__, __email__)

    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument("--version", action="version", version="{}".format(version))

    parser.add_argument(
        "str_file",
        metavar="CHIP-FILE",
        help='Delimited file. CHIP file from GSEA/MSigDB.',
    )
    parser.add_argument(
        "str_file_gmt",
        metavar="GMT-FILE",
        help='Delimited file. GMT with symbols.',
    )
    parser.add_argument(
        "str_file_map",
        metavar="MAP-FILE",
        help='Mapping file between name andexternal id.',
    )
    parser.add_argument(
        "-o",
        "--out",
        metavar="STRING",
        dest="outfile_name",
        default=None,
        help='Out-file. [default: "stdout"]',
    )

    # if no arguments supplied print help
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args, parser


def load_file(filename):
    """ LOADING FILES """
    if filename in ["-", "stdin"]:
        filehandle = sys.stdin
    elif filename.split(".")[-1] == "gz":
        filehandle = gzip.open(filename, "rt")
    elif filename.split(".")[-1] == "bz2":
        filehandle = bz2.open(filename, "rt")
    elif filename.split(".")[-1] == "zip":
        filehandle = zipfile.ZipFile(filename)
    else:
        filehandle = open(filename)
    return filehandle


def main():
    """ The main funtion. """
    args, parser = parse_cmdline()

    try:
        fileobj = load_file(args.str_file)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file))

    # create outfile object
    if not args.outfile_name:
        outfileobj = sys.stdout
    elif args.outfile_name in ["-", "stdout"]:
        outfileobj = sys.stdout
    elif args.outfile_name.split(".")[-1] == "gz":
        outfileobj = gzip.open(args.outfile_name, "wt")
    elif args.outfile_name.split(".")[-1] == "bz2":
        outfileobj = bz2.BZ2File(args.outfile_name, "wt")
    else:
        outfileobj = open(args.outfile_name, "w")

    # delimited file handler
    dMap = {}
    for a in csv.reader(load_file(args.str_file_map), delimiter="\t"):
        if a[1] != "":
            dMap[a[0]] = a[1]

    # delimited file handler
    csv_reader_obj = csv.reader(fileobj, delimiter="\t")
    header = next(csv_reader_obj)
    dSym2Ens = {}
    for a in csv_reader_obj:
        sym = a[1]
        ens = a[0]
        if sym not in dSym2Ens:
            dSym2Ens[sym] = set()
        dSym2Ens[sym].add(ens)

    try:
        fileobj = load_file(args.str_file_gmt)
    except IOError:
        error('Could not load file "{}". EXIT.'.format(args.str_file_gmt))

    csv_reader_obj = csv.reader(fileobj, delimiter="\t")
    for a in csv_reader_obj:
        idx = a[0]
        if idx in dMap:
            desc = dMap[idx] # this is the extermnal id, e.g. GO id
        else:
            desc = a[1] # this is the msigdb url
        ens = []
        for sym in a[2:]:
            ens += dSym2Ens.get(sym, []) 
        ens = list(set(ens))
        ens.sort()
        outfileobj.write("{}\t{}\t{}\n".format(idx, desc, "\t".join(ens)))

    # ------------------------------------------------------
    outfileobj.close()
    return


if __name__ == "__main__":
    sys.exit(main())
