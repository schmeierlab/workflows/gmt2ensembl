# Snakemake workflow: gmt2ensembl

[![Snakemake](https://img.shields.io/badge/snakemake-≥5.7.0-brightgreen.svg)](https://snakemake.bitbucket.io)

Transform GMT-files in symbol format to ensembl format for use of GMT-files in
RNA-seq pipelines using Ensembl/Gencode gene models.
Very simple workflow runs one scripts on each GMT-file in a input directory and 
converts the symbols to ensembl gene identifier (e.g. ENSG...) based on a Broad
MSigDB/GSEA CHIP files (see `data/README.md`).


## Authors

* Sebastian Schmeier (@s-schmeier)

## Usage

### Step 1: Install workflow

Clone or download and extract the reo. 


### Step 2: Configure workflow

Configure the workflow according to your needs via editing the file `config.yaml`.

- define input directory with GMT-files to transform (will be scanned for files of the form: `{name}.symbols.gmt`)
- location of CHIP-file (see `data/README.md`)
- define output directory


### Step 3: Execute workflow

Test your configuration by performing a dry-run via

    snakemake -np --configfile config.yaml

Run it:

    snakemake -p --configfile config.yaml

If you want to fix the underlying OS, use

    snakemake -p --use-singularity --configfile config.yaml

