# Input data description

Due to lisencing files cannot be provided here directly.
However, they are easily downloaded from the Broad or other sources, e.g.

- Download the CHIP file from [https://data.broadinstitute.org/gsea-msigdb/msigdb/annotations_versioned/](https://data.broadinstitute.org/gsea-msigdb/msigdb/annotations_versioned/), e.g. `Human_ENSEMBL_Gene_ID_MSigDB.v7.0.chip`.
- Download GMT-files for example from [MSigDB](http://software.broadinstitute.org/gsea/downloads.jsp).
