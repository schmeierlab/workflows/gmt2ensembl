## =============================================================================
## WORKFLOW PROJECT: gmt2ensembl
## INIT DATE: 20191026
import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath
from snakemake.utils import validate, min_version

## =============================================================================
## set minimum snakemake version #####
min_version("5.7.0")

# Allow users to fix the underlying OS via singularity.
singularity: "docker://continuumio/miniconda3"

## LOAD VARIABLES FROM CONFIGFILE
## submit on command-line via --configfile
if config=={}:
    print('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

## Validate configfile with yaml-schema
#validate(config, schema="schemas/config.schema.yaml")

DIR_BASE = abspath(config["outdir"])
DIR_RES = join(DIR_BASE, "results")
DIR_LOGS = join(DIR_BASE, "logs")
DIR_BM = join(DIR_BASE, "benchmarks")

## =============================================================================
## FILES to process
names, = glob_wildcards(join(config["indir"], "{name}.symbols.gmt"))
TARGETS = expand(join(DIR_RES, "{name}.ensembl.gmt"), name=names)
print(TARGETS)

## =============================================================================
## RULES
## =============================================================================
rule all:
    input:
        TARGETS

rule name2extidx:
    input:
        config["xml"]
    output:
        join(DIR_RES, "idmap.tsv.gz")
    log:
        join(DIR_LOGS, "name2extidx.log")
    benchmark:
        join(DIR_BM, "name2extidx.txt")
    shell:
        "python scripts/parse_xml.py -o {output} {input} > {log} 2>&1"


rule sym2ens:
    input:
        chip=config["chip"],
        gmt=join(config["indir"], "{name}.symbols.gmt"),
        idmap=join(DIR_RES, "idmap.tsv.gz")
    output:
        join(DIR_RES, "{name}.ensembl.gmt")
    log:
        join(DIR_LOGS, "{name}.log")
    benchmark:
        join(DIR_BM, "{name}.txt")
    shell:
        "python scripts/2ens.py -o {output} "
        "{input.chip} {input.gmt} {input.idmap} > {log} 2>&1"



